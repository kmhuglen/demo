﻿Function Get-TraceRoute
{
<#
  .SYNOPSIS
  Get-TraceRoute uses the Test-NetConnection cmdlet if available and Tracert.EXE if not.
  Displays diagnostic information for a connection.
   .EXAMPLE
  Get-TraceRoute
  Uses the default internetbeacon.msedge.net as taget
   .EXAMPLE
  Get-TraceRoute -LogonServer
  Use the LogonServer environment variabel as taget.
   .EXAMPLE
  Get-TraceRoute -Target www.microsoft.com -MaxHops 10 -DNSLookup -IPversion 6
  Uses www.microsoft.com IPv6 as target, stops after 10 hops and lookup all IP adresses.
   .PARAMETER Target
  The host to use as target, ignored if used in combination with -LogonServer
   .PARAMETER LogonServer
  Use the LogonServer environment variabel as taget.
   .PARAMETER MaxHops
  Define number of hops to try
   .PARAMETER DNSLookup
  Used to display DNS name of hops.
  Ignored if Test-NetConnection is available
   .PARAMETER IPversion
  Can be used to spesify IPv4 or IPv6
  Ignored if Test-NetConnection is available
  #>
    [CmdletBinding()]
    Param(
        [switch]
        $LogonServer
        ,
        [Parameter(ValueFromPipeline=$True)]
        [string]
        $Targets = 'internetbeacon.msedge.net'
        ,
        [int]
        $MaxHops = 30
        ,
        [switch]
        $DNSLookup
        ,
        [ValidateSet("6","4")]
        [int]
        $IPversion = 4
    )
    PROCESS {
        Foreach ($Target in $Targets) 
        {
            IF ($LogonServer) 
            {
                $Target = ($env:LOGONSERVER.Replace('\\',''))
            }
            IF ($DNSLookup) 
            { 
                $DNSLookupParameter = '' 
                $DNSLookupVerboseText = 'with DNS lookup'
            }
            Else
            {
                $DNSLookupParameter = '-d'
                $DNSLookupVerboseText = 'without DNS lookup'
            }
    
            IF (Get-Command Test-NetConnection)
            {
                Write-Verbose 'Command Test-NetConnection available'
                IF ($DNSLookup) { Write-Verbose ' Argument DNSLookup ignored' }
                IF ($IPversion -eq 6) { Write-Verbose ' Argument IPversion ignored' }
                Write-Verbose "Test-NetConnection -TraceRoute -Hops $MaxHops -ComputerName $Target -Verbose"
                Test-NetConnection -TraceRoute -Hops $MaxHops -ComputerName $Target -Verbose
            }
            Else 
            {
                Write-Verbose 'Command Test-NetConnection not available, using legacy Tracert.exe'
                Write-verbose "Getting IPv$IPversion trace route for target $Target $DNSLookupVerboseText"
                tracert.exe -h $MaxHops $DNSLookupParameter -$IPversion $Target
            }
        }
    }
}